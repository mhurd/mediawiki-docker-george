# End of automatically generated settings.
# Add more configuration options below.

wfLoadExtension( 'CheckUser' );
$wgGroupPermissions['sysop']['checkuser'] = true;
$wgGroupPermissions['sysop']['checkuser-log'] = true;
$wgGroupPermissions['sysop']['investigate'] = true;
$wgCheckUserEnableSpecialInvestigate = true;

$wgAutoCreateTempUser['enabled'] = true;
// $wgGroupPermissions['checkuser-temporary-account']['checkuser-temporary-account'] = true;
$wgGroupPermissions['sysop']['checkuser-temporary-account'] = true;
$wgGroupPermissions['*']['checkuser-temporary-account-enable'] = true;
$wgCheckUserMaximumRowCount = 3;

wfLoadExtension( 'IPInfo' );
$wgGroupPermissions['*']['ipinfo'] = true;
$wgGroupPermissions['*']['ipinfo-view-basic'] = true;
$wgGroupPermissions['*']['ipinfo-view-full'] = true;
$wgGroupPermissions['*']['ipinfo-view-log'] = true;

wfLoadExtension( 'Phonos' );

$wgPhonosEngine = "google"; 
$wgPhonosApiKeyGoogle = "AIzaSyB6sNgMkxsx7qoJtZ_u3bDuAHFqycbK_0k";
# $wgPhonosApiEndpointGoogle = "https://texttospeech.googleapis.com/v1/text:synthesize";
$wgPhonosAudioGenerationEnabled = false;
$wgPhonosStoreFilesAsMp3 = true;

# Testing Phonos Default Properties
# $wgPhonosWikibaseProperties = [
#	"wikibasePronunciationAudioProp" => "P443",
#	"wikibaseLangNameProp" => "P407",
#	"wikibaseIETFLangTagProp" => "P305"];


wfLoadExtension( 'EventLogging' );
$wgEventLoggingBaseUri = '/beacon/event';
$wgEventLoggingServiceUri = '/beacon/intake-analytics';
$wgEventLoggingStreamNames = false;

wfLoadExtensions( [
	'EventBus',
	'EventStreamConfig',
	'EventLogging',
	'WikimediaEvents'
] );
  
// EventBus Configuration
// ======================

// Send all events produced on the server to the event intake service, including
// events produced by \EventLogging::submit().
$wgEventServices = [
'*' => [ 'url' => 'http://eventlogging:8192/v1/events' ],
];
$wgEventServiceDefault = '*';
$wgEnableEventBus = 'TYPE_EVENT';

// EventStreamConfig Configuration
// ===============================

// When $wgEventLoggingStreamNames is false (not falsy), the EventLogging
// JavaScript client will treat all streams as if they are configured and
// registered.
$wgEventLoggingStreamNames = false;

// EventLogging Configuration
// ==========================

$wgEventLoggingServiceUri = "http://localhost:8192/v1/events";

// The EventLogging JavaScript client maintains an queue of events to send to
// the event intake service (see $wgEventLoggingServiceUri above). The queue is
// flushed every $wgEventLoggingQueueLingerSeconds seconds.
//
// 1 second is just long enough for you to begin to doubt that your code is
// working...
$wgEventLoggingQueueLingerSeconds = 1;

wfLoadExtension( 'GlobalBlocking' );
$wgGlobalBlockingDatabase = 'globalblocking';

#Post Local Settings Back-up of 12OCT2022
$wgImportSources = ['wikipedia'];
$wgCheckUserEnableSpecialInvestigate = true;

$wgExpensiveParserFunctionLimit = 10;

$wgImportSources = ['wikipedia'];

// Wikipedia started using in 2010 for desktop users, and so it is sometimes called the 2010 wikitext editor.
// wfLoadExtension( 'WikiEditor' );
// $wgWikiEditorRealtimePreview = true;

$wgIPInfoGeoIP2EnterprisePath = '/var/www/html/w/';
// $wgIPInfoGeoLite2Prefix = '/var/www/html/w/GeoLite2-';

$wgEnableUploads = true;
$wgGroupPermissions['*']['upload'] = true;

//Added extension WikimediaMessages 11/10/22.
wfLoadExtension( 'WikimediaMessages' );

//Uncomment the two lines ($wgCdnServersNoPurge and $wgUsePrivateIPs) below so you can show your xff header ip
//To activate xff, you will still your default ip but whatever you put in the xff header will be blocked since it's been forwarded from your default to the ip you have in your xff header
$wgCdnServersNoPurge = [ '172.0.0.1/8' ];
$wgUsePrivateIPs = true;
$wgEnablePartialActionBlocks = true;
$wgApplyIpBlocksToXff = true;
$wgApplyGlobalBlocks = true;
$wgGlobalBlockingBlockXFF = true;

//To create a system block
// $wgEnableDnsBlacklist = true;
// $wgDnsBlacklistUrls = [ '103.130.145.255' ];
// $wgSoftBlockRanges = [ '103.130.145.255' ];
// $wgProxyList = [ '2001:240::' ];
// Uncomment above.  Block IP above for partial and uploads.  you must be logged out then go to Special:Upload to get the block message 

wfLoadExtension( 'UrlShortener' );


// Added UploadWizard extension on 11/17/2022 
wfLoadExtension( 'UploadWizard' );
$wgEnableUploads = true;
$wgUseImageMagick = true;
//$wgImageMagickConvertCommand = <path to your convert command>;  # Only needs to be set if different from /usr/bin/convert

// 12/7/2022 Installed Security API
wfLoadExtension( 'SecurityApi' );
// eg. http://host.docker.internal:6927
$wgSecurityApiUrl = 'http://host.docker.internal:6927';
$wgGroupPermissions['sysop']['securityapi-feed'] = true;

//that'll make a new option appear in the form at action=protect
$wgRestrictionLevels[] = 'advanced-template-editing';
$wgGroupPermissions['sysop']['advanced-template-editing'] = true;

// Specia:Mute
$wgEnableSpecialMute = true;
$wgEnableUserEmailBlacklist = true;
$wgEchoPerUserBlacklist = true;
